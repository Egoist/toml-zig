const std = @import("std");
const testing = std.testing;
const ascii = std.ascii;

pub fn isFloat(input: []const u8) bool {
    // exceptions: nan / inf
    if (std.mem.eql(u8, input, "inf") or std.mem.eql(u8, input, "-inf") or std.mem.eql(u8, input, "+inf") or std.mem.eql(u8, input, "nan") or std.mem.eql(u8, input, "-nan") or std.mem.eql(u8, input, "+nan")) {
        return true;
    }

    if ((input[0] != '+' and input[0] != '-' and !ascii.isDigit(input[0])) or !ascii.isDigit(input[input.len - 1])) {
        return false;
    }
    var dot: u8 = 0;
    var e: u8 = 0;
    for (input[1..], 0..) |c, idx| {
        if (c == '.') {
            if (!ascii.isDigit(input[idx])) {
                return false;
            }
            dot += 1;
        } else if (c == 'e' or c == 'E') {
            if (!ascii.isDigit(input[idx])) {
                return false;
            }
            e += 1;
        } else if (c == '+' or c == '-') {
            if (input[idx] != 'e' and input[idx] != 'E') {
                return false;
            }
        } else if (c == '_') {
            if (!ascii.isDigit(input[idx]) or !ascii.isDigit(input[idx + 2])) {
                return false;
            }
        } else if (!ascii.isDigit(c)) {
            return false;
        }
        if (dot > 1 or e > 1) {
            return false;
        }
    }
    if (dot == 0 and e == 0) {
        return false;
    }
    return true;
}

pub fn toFloat(comptime T: type, input: []const u8) !T {
    return try std.fmt.parseFloat(T, input);
}

test "Float Test" {
    try testing.expect(isFloat("+1.0"));
}
test "Float Test1" {
    try testing.expect(isFloat("3.1415"));
}
test "Float Test2" {
    try testing.expect(isFloat("-0.01"));
}
test "Exponent Test" {
    try testing.expect(isFloat("5e+22"));
}
test "Exponent Test1" {
    try testing.expect(isFloat("1e06"));
}
test "Exponent Test2" {
    try testing.expect(isFloat("-2E-2"));
}
test "Both Test" {
    try testing.expect(isFloat("6.626e-34"));
}

test "Not Float Test" {
    try testing.expect(!isFloat("+.0"));
}
test "Not Float Test1" {
    try testing.expect(!isFloat("0."));
}
test "Not Float Test2" {
    try testing.expect(!isFloat("0E"));
}
test "Not Float Test3" {
    try testing.expect(!isFloat("0.."));
}

test "Float Under Test" {
    try testing.expect(isFloat("6.6_2_6e-3_4"));
}
test "Float Under Test1" {
    try testing.expect(!isFloat("6.626e-34_"));
    try testing.expect(!isFloat("6.626e_34"));
}
test "Invalid Float" {
    try testing.expect(!isFloat("3.e+20"));
    try testing.expect(!isFloat("7."));
    try testing.expect(!isFloat(".7"));
}
test "Exceptions" {
    try testing.expect(isFloat("+0.0"));
    try testing.expect(isFloat("-0.0"));
    try testing.expect(isFloat("inf"));
    try testing.expect(isFloat("-inf"));
    try testing.expect(isFloat("+inf"));
    try testing.expect(isFloat("nan"));
    try testing.expect(isFloat("-nan"));
    try testing.expect(isFloat("+nan"));
}
test "Garbage Test" {
    try testing.expect(!isFloat("0ABCDE10"));
}

test "toFloat" {
    try testing.expectEqual(@as(f32, 0.12345), try toFloat(f32, "0.12345"));
}
