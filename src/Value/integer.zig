const std = @import("std");
const testing = std.testing;
const ascii = std.ascii;
const IntError = error{
    BadPrefix,
    BadBinary,
    BadOctal,
    BadHex,
    BadNumber,
    LeadingZero,
    BadLength,
    UnderscoreAfterPrefix,
    IllegalUnderscore,
    ParsingSignedInUnsigned,
};
pub fn isInteger(input: []const u8) !bool {
    // 0 Location if prefixed
    var start: usize = 0;
    if (input[input.len - 1] == '_') {
        return IntError.IllegalUnderscore;
    }
    if (input[0] == '+' or input[0] == '-') {
        start = 1;
    }
    if (input[start] >= '1' and input[start] <= '9') {
        for (input[start + 1 ..]) |c| {
            if (!ascii.isDigit(c) and c != '_') {
                return IntError.BadNumber;
            }
        }
    }
    // check the char after the 0 if prefixed
    if (input[start] == '0' and input.len > start + 1) {
        switch (input[start + 1]) {
            'o' => {
                if (input.len == start + 2) {
                    return IntError.BadLength;
                }
                if (input[start + 2] == '_') {
                    return IntError.UnderscoreAfterPrefix;
                }
                for (input[start + 2 ..]) |c| {
                    if (!isOctal(c)) {
                        return IntError.BadOctal;
                    }
                }
            },
            'b' => {
                if (input.len == start + 2) {
                    return IntError.BadLength;
                }
                if (input[start + 2] == '_') {
                    return IntError.UnderscoreAfterPrefix;
                }
                for (input[start + 2 ..]) |c| {
                    if (c != '1' and c != '0' and c != '_') {
                        return IntError.BadBinary;
                    }
                }
            },
            'x' => {
                if (input.len == start + 2) {
                    return IntError.BadLength;
                }
                if (input[start + 2] == '_') {
                    return IntError.UnderscoreAfterPrefix;
                }
                for (input[start + 2 ..]) |c| {
                    if (!isHexDigit(c)) {
                        return IntError.BadHex;
                    }
                }
            },
            '0'...'9' => return IntError.LeadingZero,
            else => {
                return IntError.BadPrefix;
            },
        }
    }
    // +0 -0 are valid
    if (input[start] == '0' and input.len == start + 2) {
        return true;
    }
    return true;
}

fn parseUnsigned(comptime T: type, input: []const u8) !T {
    if (input[0] == '-') {
        return IntError.ParsingSignedInUnsigned;
    }
    // we already know that 0x and 0b are invalid at this point
    if (input[1] == '0' and (input[0] == '+' or input[1] == '-')) {
        return 0;
    }
    return std.fmt.parseInt(T, input, 0);
}

pub fn parseSigned(comptime T: type, input: []const u8) !T {

    // if (input[1] == '0' and (input[0] == '+' or input[1] == '-')) {
    //     return 0;
    // }
    return std.fmt.parseInt(T, input, 0);
}

inline fn isHexDigit(byte: u8) bool {
    return byte >= 'a' and byte <= 'f' or byte >= 'A' and byte <= 'F' or byte >= '0' and byte <= '9' or byte == '_';
}

inline fn isOctal(byte: u8) bool {
    return byte >= '0' and byte <= '7' or byte == '_';
}

test "is Valid Octal" {
    try testing.expect(try isInteger("0o01234567"));
}
test "is Valid Octal with _" {
    try testing.expect(try isInteger("0o012_34_567"));
}
test "is InValid Octal" {
    try testing.expectError(IntError.BadLength, isInteger("0o"));
}
test "Leading Zero Error" {
    try testing.expectError(IntError.LeadingZero, isInteger("+01"));
}

test "is Valid Binary" {
    try testing.expect(try isInteger("0b11010110"));
}
test "Valid 0" {
    try testing.expect(try isInteger("0"));
    try testing.expect(try isInteger("+0"));
    try testing.expect(try isInteger("-0"));
}
test "is Valid Hex" {
    try testing.expect(try isInteger("0xdead_beef"));
}

test "is InValid Binary" {
    try testing.expectError(IntError.IllegalUnderscore, isInteger("0b11010110_"));
}
test "TEMP BUILTIN TEST" {
    const hex3 = "0xdead_beef";
    try testing.expectEqual(parseUnsigned(u32, hex3), 0xdeadbeef);
    try testing.expectEqual(parseSigned(u32, hex3), 0xdeadbeef);
}

test "WHAT" {
    const t = "1234";
    try testing.expect(try isInteger(t));
}

test "parsing of Zero" {
    const hex3 = "+0";
    try testing.expectEqual(parseUnsigned(u32, hex3), 0);
    const hex0 = "-0";
    try testing.expectEqual(parseSigned(u32, hex0), 0);
}
