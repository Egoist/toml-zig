const std = @import("std");
const ArrayList = std.ArrayList;
const value = @import("value.zig");

const test_string = @embedFile("config.toml");

const Frnds = struct {
    Email: []const u8,
};

const Tml = struct { Friends: []Frnds };

const Toml = struct {
    Toml: []Tml,
};

pub fn main() !void {
    const start_time = std.time.nanoTimestamp();
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    //var stream = value.TokenStream.init(test_string);
    //var val = try stream.get(allocator, "Toml");
    //defer val.deinit(allocator);
    var kk = try value.parse(Toml, allocator, test_string);

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();
    //std.debug.print("{s}\n", .{val.Array.items[4].Table.get("Friends").?.Array.items[10].Table.get("Email").?.String});
    try stdout.print("{s}\n", .{kk.Toml[4].Friends[4].Email});
    try stdout.print("elapsed nano seconds: {}\n", .{std.time.nanoTimestamp() - start_time});
    try bw.flush(); // don't forget to flush!
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
